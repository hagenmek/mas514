===================
Lab Exercise
===================

Exercise #2
===================


******************************
Problem 1
******************************

Derive the equation of motions for the differential drive robot showed in the figure below


.. figure:: ../figs/labexercise/ex2_problem1.png
    :width: 300px
    :align: center


* Two fixed standard wheels
* The robot frame (R) in between the wheels
* Stack the wheel equation for this configuration

Find (see Chapter 3.2, Autonomous Mobile Robots):
a. Forward differential kinematics
b. Inverse differential kinematics
c. Degree of maneuverability

Solution proposal: https://www.youtube.com/watch?v=tymqJPNjyU0&list=PL0m60CTll3OwWqpck32Z6Tzc8_0BxfYOH&index=12

------------------
1a - Solution
------------------

Forward differential kinematics

.. math::
    \begin{bmatrix}
    \dot{x}\\
    \dot{y}\\
    \dot{\theta}
    \end{bmatrix}
    =
    \begin{bmatrix}
    r/2 & r/2\\
    0 & 0\\
    r/2b & -r/2b
    \end{bmatrix}
    \begin{bmatrix}
    \dot{\varphi_r}\\
    \dot{\varphi_l}
    \end{bmatrix}



------------------
1b - Solution
------------------

Inverse differential kinematics

Solution

.. math::
    \begin{bmatrix}
    \dot{\varphi_r}\\
    \dot{\varphi_l}
    \end{bmatrix}
    =
    \begin{bmatrix}
    1/r & 0 & b/r\\
    1/r & 0 & -b/r
    \end{bmatrix}
    \begin{bmatrix}
    \dot{x}\\
    \dot{y}\\
    \dot{\theta}
    \end{bmatrix}

------------------
1c - Solution
------------------
Degree of Maneuverability

Solution

.. math::
    \delta_m = 2, \mspace{30mu} \delta_s = 0, \mspace{30mu} \delta_M = 2


******************************
Problem 2
******************************

Home (extra) Work: Kinematics and control of the JetBot

The goal of this exercise is to program and implement a closed-loop motion controller for a differential-drive robot.
For this, two subtasks have to be solved:

a. Given desired forward and angular velocities of the robot, one should compute the corresponding wheel velocities in
order to make the robot drive accordingly
b. Develop a controller that computes velocity commands to drive the robot to a specified target position (see Chapter
3.6, Autonomous Mobile Robots)


.. figure:: ../figs/labexercise/ex2_Problem2.png
    :width: 400px
    :align: center


------------------
2a - Solution
------------------
Inverse Differential Kinematics

Solution

See MATLAB livescript solution proposal for problem 1abc in Canvas to see how the equations below are derived and
isolated

``https://uia.instructure.com/courses/9411/files/1528790/download?download_frd=1``

.. math::
    \dot{\varphi}_l = \frac{v + b \omega}{r} \\
    \dot{\varphi}_r = \frac{v - b \omega}{r}

Inverse Kinematics Control is implemented in a new node :code:`InverseKinematics.py` as shown below

.. literalinclude:: ../../src/InverseKinematics.py
    :language: python

Modify the following highlighted values to change the velocity in x direction [m/s] and the angular rotation arround
robot frames origo [rpm]

.. figure:: ../figs/labexercise/ex2_Problem2_1.png
    :width: 200px
    :align: center

The old ros launch file :code:`start.launch` is modified as shown in the branch for Problem 2a. In the A2021 branch, the ned node is commented out as shown below:

.. literalinclude:: ../../launch/start.launch
    :language: xml

The new InverseKinematics node including the modified ``start.launch`` is updated in a new git branch
``https://gitlab.com/hagenmek/mas514/-/tree/Ex2_Problem2a`` while the original launch file is found in the A2021 branch.

It is also possible to switch between branches directly in VS code

.. figure:: ../figs/labexercise/switch_branch_inVScode.png
    :width: 600px
    :align: center

If the new branch is not listed, try to delete the git folder and clone again
``git clone https://gitlab.com/hagenmek/mas514``


------------------
2b
------------------

First problem 3a must be carried out for reading wheel encoder position to estimate the robots pose.


Exercise #3
===================

******************************
Problem 1
******************************

Install Arduino IDE from Canvas (https://uia.instructure.com/courses/9411/files/folder/Arduino). Also available for Mac
and Linux (https://www.arduino.cc/en/software) If problem with USB drive not detecting the Arduino as a COM-port install
CDM212364_Setup.exe that is also available in Canvas.

.. figure:: ../figs/labexercise/Ex3_Problem2.png
    :width: 500px
    :align: center

It is also possible to program the Arduiono from VS code as described here: https://www.youtube.com/watch?v=VfLTZcKCGfk.
However, the serial-monitor did not work verry well for this exercise. So it is recomended to do 1c from the Arduino
IDE.

------------------
1a
------------------

Download the Parallax-FeedBack-360-Servo-Control-Library.zip file from Canvas and add it to the Arduino library as shown in the picture below

.. figure:: ../figs/labexercise/Ex3_Problem1a.png
    :width: 500px
    :align: center

Open the Parallax Feedback 360 Servoe Read Example as shown below 

.. figure:: ../figs/labexercise/Ex3_Problem1a_1.png
    :width: 500px
    :align: center

.. literalinclude:: ../../Arduino/Ex3_Problem1a.ino
    :language: c++

Connect the Arduiono Nano to the JetBot as shown in the figure below

.. figure:: ../figs/labexercise/Ex3_Problem1a_2.png
    :width: 500px
    :align: center

------------------
1b
------------------

Upload the Read sketch to the Arduino and open serial-monitor.

.. literalinclude:: ../../Arduino/Ex3_Problem1b/Ex3_Problem1b.ino
    :language: c++  

Run the JetBot motors through ROS using the virtual JoyStick or similar.

The angular rotation should be continously updated for the motor connected to pin D2 (left motor) when driving the motor.

------------------
1c
------------------

Modify the code to read also the encoder for the second motor (right motor).

------------------
1c - Solution
------------------

I have added the whole code in one file, no need for library, .h or .cpp file.

.. literalinclude:: ../../Arduino/Ex3_Problem1c_Solution/Ex3_Problem1c_Solution.ino
    :language: c++   

Fore more details about the code take a look at the datesheet in Canvas: https://uia.instructure.com/courses/9411/files/folder/JetBot/Data%20sheets?preview=1524097

******************************
Problem 2
******************************

Ultrasonic Range Sensor

Measure the distance to an object using the HC-SR04

2a. Set-up the HC-SR04 according to the guide below (1a)

2b. Implement the missing equation in the provided sketch

2c. Monitor/plot the measured distance

------------------
2a
------------------


Connect the UltraSonic sensor to Nano as shown in picture below

.. figure:: ../figs/labexercise/Nano_HC_SR04_setup.png
    :width: 300px
    :align: center

------------------
2b
------------------

Implement the missing equation in the provided Arduino sketch :code:`Ex3_Problem1.ino` available in Canvas
(https://uia.instructure.com/courses/9411/files/folder/Arduino) and in Git as shown below: 

.. literalinclude:: ../../Arduino/Ex3_Problem2/Ex3_Problem2.ino
    :language: c++

------------------
2b Solution
------------------

.. literalinclude:: ../../Arduino/Ex3_Problem2_Solution/Ex3_Problem2_Solution.ino
    :language: c++    

Fore more details about the code take a look at this video: https://www.youtube.com/watch?v=ZejQOX69K5M&t=139s
------------------
2c
------------------

Monitor/plot the measured distance (can not be done at the same time).

******************************
Problem 3 - Extra / Home work
******************************

Please skip problem 3c until a guide is provided. Some groups (including myself) have problems getting the JetBot camera working after installing the real sense ROS wrapper. 

Intel RealSense L515 | LiDAR Camera (2D/3D)  

3a. Get to know the L515 LiDAR Camera. Take a look here: https://www.intelrealsense.com/lidar-camera-l515/

3b. Download Intel RealSense SDK 2.0 (Intel RealSense SDK 2.0) and play with the L515 LiDAR Camera

3c. Connect the L515 LiDAR Camera to the Jetson Nano and explor the ROS wrapper https://dev.intelrealsense.com/docs/ros-wrapper


Exercise #4
===================

Arduino and IMU

******************************
Problem 1
******************************

Connect the Arduino NANO and the MPU-9250 (IMU) according to the wiring diagram below.

.. figure:: ../figs/labexercise/Ex4_Problem1.png
    :width: 500px
    :align: center

------------------
1a
------------------

Open the prepared Arduino sketch (as shown below) and the incuided .cpp and .h file from git (https://gitlab.com/hagenmek/mas514/-/tree/A2021/Arduino/Ex4_Problem1a) reading the IMU values for the 9-axis and the temperature. 

.. literalinclude:: ../../Arduino/Ex4_Problem1a/Problem1a.ino
    :language: c++   

The header file from the original library (https://github.com/bolderflight/mpu9250-arduino) is modified such that the orientation in XYZ is matching the labeling on the IMU we have as shown in picture below:

.. figure:: ../figs/labexercise/Ex4_Problem1a.png
    :width: 250px
    :align: center


Downlod SerialPlot from Canvas (https://uia.instructure.com/files/1558584/download?download_frd=1) and plot the printed Acceleration from the Accelerometer and the Angular velicy from the Gyrscope as described below and check if the directions are according to the figure above.

* Select correct port and baud rate accrodign to the Arduino sketch
.. figure:: ../figs/labexercise/Ex4_Problem1a_1.png
    :width: 500px
    :align: center


* Select ASCII, Auto number of channels, and comma coloumn delimiter
.. figure:: ../figs/labexercise/Ex4_Problem1a_2.png
    :width: 500px
    :align: center


* Click on Open to start the plotting. Specify name of the differ values according to the naming and order of the Arduino Sketch
.. figure:: ../figs/labexercise/Ex4_Problem1a_3.png
    :width: 500px
    :align: center


* Adjust Width and Buffer size to adjust the time axis. Define XY scaling if Auto is not optimal. 
* Only one serial monitor can be active, the SerialPlot must be deactivated (re-select Open) to upload new code   

------------------
1b
------------------

Estimate yaw, pitch, and roll by integrating the measured angular velocity of the gyroscope. Plot the values using SerialPlot and check if the directions are correct according to figure above.

Se example below on how to implement integration in Arduino:

.. literalinclude:: ../../Arduino/Ex4_Problem1b/Problem1b.ino
    :language: c++   

------------------
1c
------------------

Visualize the yaw, pitch, and roll motion using the 3D Visualizer (Precessing 4) by folowing the steps below:

* Download and unzip the program from Canvas (https://uia.instructure.com/files/1558642/download?download_frd=1) 
* Open the .pde file from git: https://gitlab.com/hagenmek/mas514/-/tree/A2021/Arduino/Ex4_Problem1c/Problem1c
  
.. literalinclude:: ../../Arduino/Ex4_Problem1c/Problem1c.ino
    :language: c++  

* Make sure the Arduino program derived in 1b is running and printing the float values of yaw, pitch, roll. 
* Comment out the serial plots that is not relevant for better performanc.
* Orient the IMU to match the orientation figure from the lecture
* 
  .. figure:: ../figs/labexercise/Ex4_Problem1c_1.png
    :width: 500px
    :align: center
 
* Adjust COM port and baud rate (use 19200 for smoother animation) to match the Arduino program and click play to run the Visualizer
  
  .. figure:: ../figs/labexercise/Ex4_Problem1c.png
    :width: 500px
    :align: center
    
  .. figure:: ../figs/labexercise/Ex4_Problem1c_2.png
    :width: 500px
    :align: center

* Click restart button on the Arduino Nano to start from inital position.

* Only one serial monitor can be active, the 3D vizualiser must be closed to upload new code   

------------------
1d
------------------

Initially there is error in the measured gyroscope values. 

* Run the calibration script available in git (https://gitlab.com/hagenmek/mas514/-/tree/A2021/Arduino/Ex4_Problem1d). 
* Have the IMU flat on the ground and keep it steady without holding it.
* Print the error values and make a note. Press reset button on the Nano to get new measurment.
* Implement a calibration function into the script from problem 1b by adding/subtracting the measured error to the original angular velocities (i.e., GyroX, GyroY, GyroZ).
* Verify the improvement with the 3D visualizer. When reseting the Nano the vizualised values should start from zero.

------------------
1e
------------------

* Estimate the pitch and roll using the accelerometer and implement it in the Arduino code. Remember to normalize the acceleration by deviding the accelerations with the gravitational acceleration (g). Include math.h to use the atan2 function.
* Expand the calibration sketch to calculate the error of the pitch and roll of the accelerometer and implement the calibratio function as in problem 1d.
* Visualize the pitch and roll (just set yaw to zero) and verify that the directions are correct.
* Compare the estimated values with the values from the gyroscope using SerialPlot.

------------------
1f
------------------

Implement a complementary filter and tune the parameter to remove drift from the pitch and roll movement. Use the 3D vizualiser to show the effect. 

------------------
1g - Extra/Homework
------------------

Implement a kalman filter. Ceck out this YouTube series for help: https://www.youtube.com/watch?v=Os6V1lnUPZo

------------------
1h - Extra/Homework
------------------

To fully eliminate drift in yaw we can either implement the magnometer which can be used as a long-term correction for the gyroscope Yaw drift or we can apply the Digital Motion Processor of the IMU which is used for onboard calculations of the data and it’s capable of eliminating the Yaw drift as mentioned in this tutorial: https://howtomechatronics.com/tutorials/arduino/arduino-and-mpu6050-accelerometer-and-gyroscope-tutorial/ using the  i2cdevlib library by Jeff Rowberg https://github.com/jrowberg/i2cdevlib.

******************************
Problem 2 - Extra/Homework
******************************
 
Use the IMU to estimate the position of the mobile robot with respect to the inertial frame. You don’t need to integrate it on the JetBot, just introduce movement by moving the IMU on the ground
Use the Arduino code from problem 1. 


  .. figure:: ../figs/labexercise/Ex4_Problem3a.png
    :width: 500px
    :align: center

  .. figure:: ../figs/labexercise/Ex4_Problem3a_1.png
    :width: 300px
    :align: center

Exercise #6
===================

TurtleBot3 SLAM Simulation and preperation for doing SLAM and navigation on the JetBot


******************************
Problem 1
******************************

TurtleBot3 SLAM Simulation

------------------
1a - Setup
------------------

1. Install Dependent ROS Packages by copy the following into the terminal:
   
    ``sudo apt-get install ros-melodic-joy ros-melodic-teleop-twist-joy
    ros-melodic-teleop-twist-keyboard ros-melodic-laser-proc 
    ros-melodic-rgbd-launch ros-melodic-depthimage-to-laserscan 
    ros-melodic-rosserial-arduino ros-melodic-rosserial-python 
    ros-melodic-rosserial-server ros-melodic-rosserial-client 
    ros-melodic-rosserial-msgs ros-melodic-amcl ros-melodic-map-server  
    ros-melodic-move-base ros-melodic-urdf ros-melodic-xacro 
    ros-melodic-compressed-image-transport ros-melodic-rqt* 
    ros-melodic-gmapping ros-melodic-navigation ros-melodic-interactive-markers``

2. Install TurtleBot3 Packages:     
   
   * ``sudo apt-get install ros-melodic-dynamixel-sdk``
   * ``sudo apt-get install ros-melodic-turtlebot3-msgs``
   * ``sudo apt-get install ros-melodic-turtlebot3``
    
3. Download and build code:   
      
   * ``sudo apt-get remove ros-melodic-dynamixel-sdk`` 
   * ``sudo apt-get remove ros-melodic-turtlebot3-msgs`` 
   * ``sudo apt-get remove ros-melodic-turtlebot3`` 
   * ``cd ~/catkin_ws/src/``   
   * ``git clone -b melodic-devel https://github.com/ROBOTIS-GIT/DynamixelSDK.git``    
   * ``git clone -b melodic-devel https://github.com/ROBOTIS-GIT/turtlebot3_msgs.git``     
   * ``git clone -b melodic-devel https://github.com/ROBOTIS-GIT/turtlebot3.git``      
   * ``cd ~/catkin_ws && catkin_make``    
   * ``echo "source ~/catkin_ws/devel/setup.bash" >> ~/.bashrc``    

------------------
1b - Gazebo Simulation
------------------

1. Install Simulation Package:
   
   * ``cd ~/catkin_ws/src/``    
   * ``git clone -b melodic-devel https://github.com/ROBOTIS-GIT/turtlebot3_simulations.git``        
   * ``cd ~/catkin_ws && catkin_make``  

2. Launch Simulation World:
   
   * ``export TURTLEBOT3_MODEL=burger`` 
   * ``roslaunch turtlebot3_gazebo turtlebot3_world.launch``    

3. Operate TurtleBot3:

    In order to teleoperate the TurtleBot3 with the keyboard, launch the teleoperation node with below command in a new terminal window.

    * ``export TURTLEBOT3_MODEL=burger``

   * ``roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch``

   * w/x : increase/decrease linear velocity
   * a/d : increase/decrease angular velocity
   * s : force stop

4. Visualize Simulation data in RViz:
   
    RViz visualizes published topics while simulation is running. You can launch RViz in a new terminal window by entering below command:

    * ``export TURTLEBOT3_MODEL=burger``

   * ``roslaunch turtlebot3_gazebo turtlebot3_gazebo_rviz.launch``

------------------
1c - SLAM Simulation
------------------

Gmapping SLAM method is used by default.

First, make sure no other simulations are running. Terminate terminals that are running by entering ``Ctrl+c`` in the terminal.

1. Launch Simulation World in a new terminal window:
   
   * ``export TURTLEBOT3_MODEL=burger``
   * ``roslaunch turtlebot3_gazebo turtlebot3_world.launch``

2. Run SLAM Node in a new terminal window:: 
        
   * ``export TURTLEBOT3_MODEL=burger``
   * ``roslaunch turtlebot3_slam turtlebot3_slam.launch slam_methods:=gmapping``

3. Operate TurtleBot3:

    In order to teleoperate the TurtleBot3 with the keyboard, launch the teleoperation node with below command in a new terminal window.

   * ``export TURTLEBOT3_MODEL=burger``
   * ``roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch``

   * w/x : increase/decrease linear velocity
   * a/d : increase/decrease angular velocity
   * s : force stop    

 4. Save Map:

    When the map is created successfully, open a new terminal from Remote PC with and save the map

   * ``rosrun map_server map_saver -f ~/map``

    Try to find the saved map.

------------------
1d Extra/Home work
------------------

To learn more about Gmapping and the Rao-Blackwellized particle filter see: 

https://openslam-org.github.io/gmapping.html (see Papers Describing the Approach and Further Reading)


The Gmapping ROS package:

http://wiki.ros.org/gmapping


******************************
Problem 2
******************************

Set-up L515 LiDAR Camera on the JetBot.

This problem is the same as given in Ex #3 - Problem 3.

See guide here: https://hagenmek.gitlab.io/mas514/src/L515.html#install-realsense2-ros


******************************
Problem 3
******************************

Convert the pointcloud from the L515 LiDAR Camera to a Laser Scan that can be used with ROS Gmapping and ROS Navigation stack.

See guide here: https://hagenmek.gitlab.io/mas514/src/L515.html#pointclound-to-laserscan

Exercise #7
===================

TurtleBot3 Navigation Simulation and preperation for doing SLAM and navigation on the JetBot


******************************
Problem 1
******************************

TurtleBot3 Navigation Simulation (this must be done from the Jetson Nano terminal)

Just like the SLAM in Gazebo simulator, you can select or create various environments and robot models in virtual Navigation world. However, proper map has to be prepared before running the Navigation. 

------------------
1a - Launch Simulation World 
------------------

In the previous SLAM section, TurtleBot3 World is used to creat a map. The same Gazebo environment will be used for Navigation.

Please use the proper keyword among burger, waffle, waffle_pi for the TURTLEBOT3_MODEL parameter.

   * ``export TURTLEBOT3_MODEL=burger``
   * ``roslaunch turtlebot3_gazebo turtlebot3_world.launch``

------------------
1b - Run Navigation Node 
------------------

Open a new terminal from Remote PC with Ctrl + Alt + T and run the Navigation node. If you dont have a map saved of the correct world saved from exercise 6  - Problem 1c, do this exercise again or get a map from other classmated that has this located here: $HOME/map.yaml  

   * ``export TURTLEBOT3_MODEL=burger``
   * ``roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=$HOME/map.yaml``

------------------
1c - Estimate Initial Pose 
------------------

Initial Pose Estimation must be performed before running the Navigation as this process initializes the AMCL parameters that are critical in Navigation. TurtleBot3 has to be correctly located on the map with the LDS sensor data that neatly overlaps the displayed map.

1. Click the 2D Pose Estimate button in the RViz menu.
2. Click on the map where the actual robot is located and drag the large green arrow toward the direction where the robot is facing.
3. Repeat step 1 and 2 until the LDS sensor data is overlayed on the saved map.
4. Launch keyboard teleoperation node (in a new terminal) to precisely locate the robot on the map.
   
   * ``export TURTLEBOT3_MODEL=burger``   
   * ``roslaunch turtlebot3_teleop turtlebot3_teleop_key.launch`` 
5. Move the robot back and forth a bit to collect the surrounding environment information and narrow down the estimated location of the TurtleBot3 on the map which is displayed with tiny green arrows.
6. Terminate the keyboard teleoperation node by entering Ctrl + C to the teleop node terminal in order to prevent different cmd_vel values are published from multiple nodes during Navigation.

------------------
1d - Set Navigation Goal
------------------

1. Click the 2D Nav Goal button in the RViz menu.
2. Click on the map to set the destination of the robot and drag the arrow toward the direction where the robot will be facing.

    * This arrow is a marker that can specify the destination of the robot.
    * The root of the arrow is x, y coordinate of the destination, and the angle θ is determined by the orientation of the arrow.
    * As soon as x, y, θ are set, TurtleBot3 will start moving to the destination immediately.

------------------
1e - Tuning Guide
------------------

This task is mainly relevant for the project, but take a look and play with the parameters to understand what the different parameters do.

Navigation stack has many parameters to change performances for different robots.

You can get more information about Navigation tuning from Basic Navigation Tuning Guide (http://wiki.ros.org/navigation/Tutorials/Navigation%20Tuning%20Guide), ROS Navigation Tuning Guide by Kaiyu Zheng (https://kaiyuzheng.me/documents/navguide.pdf), and the chapter 11 of ROS Robot Programming book (https://community.robotsource.org/t/download-the-ros-robot-programming-book-for-free/51).

1. inflation_radius:
   
   * Defined in ``turtlebot3_navigation/param/costmap_common_param_${TB3_MODEL}.yaml``
   * This parameter makes inflation area from the obstacle. Path would be planned in order that it don’t across this area. It is safe that to set this to be bigger than robot radius. For more information, please refer to the costmap_2d wiki (http://wiki.ros.org/costmap_2d#Inflation).


2. cost_scaling_factor:
   
   * Defined in ``turtlebot3_navigation/param/costmap_common_param_${TB3_MODEL}.yaml``
   * This factor is multiplied by cost value. Because it is an reciprocal propotion, this parameter is increased, the cost is decreased.
   * The best path is for the robot to pass through a center of between obstacles. Set this factor to be smaller in order to move fare from obstacles.
  

3. max_vel_x:
   
   * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
   * This factor is set the maximum value of translational velocity.


4. min_vel_x:
   
   * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
   * This factor is set the minimum value of translational velocity. If set this negative, the robot can move backwards.


5. max_trans_vel:
    
   * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
   * Actual value of the maximum translational velocity. The robot can not be faster than this.


6. min_trans_vel:
     
   * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
   * Actual value of the minimum translational velocity. The robot can not be slower than this.


7. max_rot_vel:
    
   * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
   * Actual value of the maximum rotational velocity. The robot can not be faster than this.


8. min_rot_vel:
    
   * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
   * Actual value of the minimum rotational velocity. The robot can not be slower than this.


9.  acc_lim_x:
      
    * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
    * Actual value of the translational acceleration limit.


10. acc_lim_theta:
      
    * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
    * Actual value of the rotational acceleration limit.


11. xy_goal_tolerance:
      
    * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
    * The x,y distance allowed when the robot reaches its goal pose.


12. yaw_goal_tolerance:
    
    * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
    * The yaw angle allowed when the robot reaches its goal pose.


13. sim_time:       
      
    * Defined in ``turtlebot3_navigation/param/dwa_local_planner_params_${TB3_MODEL}.yaml``
    * This factor is set forward simulation in seconds. Too low value is in sufficient time to pass narrow area and too high value is not allowed rapidly rotates. 

------------------
1f - Extra/Home work
------------------

To learn more about ROS Navigation check out the wiki page: http://wiki.ros.org/navigation, and study the different packages included in the Navigation stack:

  .. figure:: ../figs/labexercise/navigation_stack.png
    :width: 700px
    :align: center
    
  * amcl: http://wiki.ros.org/amcl?distro=melodic
  * base_local_planner: http://wiki.ros.org/base_local_planner?distro=melodic
  * carrot_planner: http://wiki.ros.org/carrot_planner?distro=melodic
  * clear_costmap_recovery: http://wiki.ros.org/clear_costmap_recovery?distro=melodic
  * costmap_2d: http://wiki.ros.org/costmap_2d?distro=melodic
  * dwa_local_planner: http://wiki.ros.org/dwa_local_planner?distro=melodic
  * fake_localization: http://wiki.ros.org/fake_localization?distro=melodic
  * global_planner: http://wiki.ros.org/global_planner?distro=melodic
  * map_server: http://wiki.ros.org/map_server?distro=melodic
  * move_base: http://wiki.ros.org/move_base?distro=melodic
  * move_base_msgs: http://wiki.ros.org/move_base_msgs?distro=melodic
  * move_slow_and_clear: http://wiki.ros.org/move_slow_and_clear?distro=melodic
  * nav_core: http://wiki.ros.org/nav_core?distro=melodic
  * navfn: http://wiki.ros.org/navfn?distro=melodic
  * rotate_recovery: http://wiki.ros.org/rotate_recovery?distro=melodic
  * voxel_grid: http://wiki.ros.org/voxel_grid?distro=melodic

******************************
Problem 2 - Extra/Home Work
******************************

Look at the exercise (PDF in exercise zip folder) from ETH: https://ethz.ch/content/dam/ethz/special-interest/mavt/robotics-n-intelligent-systems/asl-dam/documents/lectures/autonomous_mobile_robots/spring-2021/ex6_slides.pdf

Exercise: https://ethz.ch/content/dam/ethz/special-interest/mavt/robotics-n-intelligent-systems/asl-dam/documents/lectures/autonomous_mobile_robots/spring-2021/ethzasl_amr_exercise6.zip
Solution: https://ethz.ch/content/dam/ethz/special-interest/mavt/robotics-n-intelligent-systems/asl-dam/documents/lectures/autonomous_mobile_robots/spring-2021/ethzasl_amr_solution6.zip


Pre-project Work
===================

******************************
JetBot
******************************

This task is for the groups considering using the JetBot mobile-robot in the project. 

Study the Tutorials for Setup and Configuration of the Navigation Stack on a Robot: http://wiki.ros.org/navigation/Tutorials/RobotSetup 


******************************
uARM 
******************************

This task is for the groups considering using the uArm robot manipulator in the project. 

Study the following material: https://www.ufactory.cc/pages/download-uarm, espesially the part about UFACTORY uArm ROS (https://github.com/uArm-Developer/RosForSwiftAndSwiftPro) and MovIt 1 for Ubuntu 18.04 (https://moveit.ros.org/install/)

