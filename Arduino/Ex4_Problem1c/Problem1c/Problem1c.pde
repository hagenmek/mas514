/*
    MAS514
    Arduino and MPU-9050 IMU - 3D Visualization Example 
*/
import processing.serial.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
Serial myPort;
String data="";
float yaw,pitch,roll;
void setup() {
  size (800, 600, P3D);
  myPort = new Serial(this, "COM5", 19200); // starts the serial communication
  myPort.bufferUntil('\n');
}
void draw() {
  translate(width/2, height/2, 0);
  background(155);
  textSize(22);
  text("Yaw: " + int(yaw) + "       Pitch: " + int(pitch)  +  "        Roll:" + int(roll),  -100, 265);
  // Rotate the object
  
  rotateY(radians(-yaw));
  rotateX(radians(-pitch));
  rotateZ(radians(roll));
  
  // 3D 0bject
  textSize(30);  
  fill(0, 76, 153);
  box (386, 40, 200); // Draw box
  textSize(25);
  fill(255, 255, 255);
  text("X x---> Y                MAS514", -183, 10, 101);
  //delay(10);
  //println("ypr:\t" + angleX + "\t" + angleY); // Print the values to check whether we are getting proper values
}
// Read data from the Serial Port
void serialEvent (Serial myPort) { 
  // reads the data from the Serial Port up to the character '.' and puts it into the String variable "data".
  data = myPort.readStringUntil('\n');
  // if you got any bytes other than the linefeed:
  if (data != null) {
    data = trim(data);
    // split the string at "/"
    String items[] = split(data, ',');
    if (items.length > 1) {
      //--- Yaw, Pitch, Roll in degrees
      yaw = float(items[0]);
      pitch = float(items[1]);
      roll = float(items[2]);
    }
  }
}
