/*
* MAS514
* Ex4 - Problem 1a
*/

// Include library of the IMU (MPU-9250)
#include "mpu9250.h"
// Declear Variables
float AccX,AccY,AccZ,GyroX,GyroY,GyroZ,MagX,MagY,MagZ,Temp;

/* An Mpu9250 object with the MPU-9250 sensor on I2C bus 0 with address 0x68 */
Mpu9250 imu(&Wire, 0x68);
int status;

void setup() {
  /* Serial to display data */
  Serial.begin(19200);
  while(!Serial) {}
  /* Start communication */
  if (!imu.Begin()) {
    Serial.println("IMU initialization unsuccessful");
    while(1) {}
  }
}

void loop() {
  /* Read the sensor */
  if (imu.Read()) {
    
    // Accelerometer [m/s^2]
    AccX = imu.accel_x_mps2();   
    AccY = imu.accel_y_mps2();   
    AccZ = imu.accel_z_mps2();  
    // Gyroscope [deg/s]
    GyroX = imu.gyro_x_radps()*(180/PI);   
    GyroY = imu.gyro_y_radps()*(180/PI);    
    GyroZ = -imu.gyro_z_radps()*(180/PI);   
    // Magnometer [micro Tesla]
    MagX = imu.mag_x_ut();  
    MagY = imu.mag_y_ut();    
    MagZ = imu.mag_z_ut();    
    // Temperature [deg C]
    Temp = imu.die_temperature_c(); 
     
    // Serial Plot 
    // Accelerometer
    Serial.print(AccX);
    Serial.print(",");
    Serial.print(AccY);
    Serial.print(",");
    Serial.print(AccZ);
    Serial.print(",");
    // Gyroscope
    Serial.print(GyroX);
    Serial.print(",");
    Serial.print(GyroY);
    Serial.print(",");
    Serial.print(GyroZ);
    Serial.print(",");
    // Magnometer
    Serial.print(MagX);
    Serial.print(",");
    Serial.print(MagY);
    Serial.print(",");
    Serial.print(MagZ);
    Serial.print(",");
    // Temperatur
    Serial.println(Temp);
  }
}
